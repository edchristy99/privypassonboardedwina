import html2canvas from "html2canvas"

const CapturePic = () => {
    const snap = () =>{
        html2canvas(document.body).then(function(canvas){
            var a = document.createElement('a')
            a.href = canvas.toDataURL("...assets/image/jpg").replace("image/jpg", "image/octet-system")
            a.download = `captured_edvidcall.jpg`
            a.click()
        })
    } 
    return(<button onClick={snap}>Get a Pic from edvidcall</button>);

}

export default CapturePic