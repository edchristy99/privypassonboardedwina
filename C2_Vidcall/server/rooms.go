package server

import (
	"log"
	"math/rand"
	"sync"
	// "time"

	"github.com/gorilla/websocket"
	// "golang.org/x/net/websocket"
)

//Participant describes a single entitiy in the hash map
type Participant struct{
	Host bool
	Conn *websocket.Conn
}

//RoomMap is the main host map {roomID sring } -> [[]Participant]
type RoomMap struct{
	Mutex sync.RWMutex
	Map map[string][]Participant
}

func (r *RoomMap) Init(){

	r.Map = make (map[string][]Participant)
}
//Get will return  the arry of participants in the room
func (r *RoomMap) Get(roomID string) []Participant {
	r.Mutex.Lock()
	defer r.Mutex.Unlock()
	return r.Map[roomID]
}

//generate a unique room id -> Insert in the hash map
func (r *RoomMap) CreateRoom() string{
	r.Mutex.Lock()
	defer r.Mutex.Unlock()

	var letters = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890")
	b := make([]rune, 8)
	for i := range b {
		b[i] = letters[rand.Intn(len(letters))]
	}

	roomID := string(b)
	r.Map[roomID] = []Participant{}

	return roomID

}

func (r *RoomMap) InsertIntoRoom(roomID string, host bool, conn *websocket.Conn){
	r.Mutex.Lock()
	defer r.Mutex.Unlock()

	p := Participant{host, conn}


	log.Println("Inserting into Room with Room ID: ", roomID)
	r.Map[roomID] = append(r.Map[roomID], p);

}

func (r *RoomMap) DeleteRoom(roomID string){
	r.Mutex.Lock()
	defer r.Mutex.Unlock()

	delete(r.Map, roomID)
}

