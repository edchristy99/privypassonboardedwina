/// <reference types="cypress" />
const token ='eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6ImVkY2hyaXN0eTk5IiwiX2lkIjoiNjMzMmY4NWY5MzM2ZDEwMDA5NzU0MTE2IiwibmFtZSI6IkVkd2luYSBDaHJpc3R5IiwiaWF0IjoxNjY0Mjg1MzQyLCJleHAiOjE2Njk0NjkzNDJ9.uoo65ya9nUirNKay5J6JnVWvnCflHdl4Y8-R5Q4r-o8'
      
describe ('Basic Test', ()=>{
    before(() => {
        cy.then(() =>{
            window.localStorage.setItem('__auth__token', token )
        })
    })

    beforeEach(()=>{
        cy.viewport('macbook-16')
        cy.visit('https://codedamn.com/')
       
    })
    
    it('Should Pass', () =>{})

    it('Should load playground correctly', () => {
        cy.visit("https://codedamn.com/playground/TTbcAKQE93DT1z1NjMIhY");
        cy.log("Checking for sidebar")
       cy.contains('Check bottom left button').should('exist')
       cy.get('[data-testid=xterm-controls] > div').should('contain.text', 'Connecting')

        cy.get('Trying to establish connecting').should('exist')

        cy.log('Playground is initializing')
       cy.get('Setting up the challenge').should('exist')
        cy.get('Setting up the challenge', { timeout: 1 * 1000 }).should('not.exist') //auto failed when its timeout 
      })
    
      
      it.only('New file feature works', () => {
        cy.visit("https://codedamn.com/playground/TTbcAKQE93DT1z1NjMIhY");
        cy.log('Checking for sidebar')
       
        // cy.get('Setting up the challenge').should('exist')
        // cy.get("Setting up the challenge", { timeout: 7 * 1000 }).should('exist')
        
        // cy.get("Setting up the challenge", { timeout: 7 * 1000 }).should('not.exist')
        
        //cy.get(['class=xterm-helper-textarea']).type('{ctrl}{c}')
        cy.contains('script.js').should('exist')

     })


})
