//let -->re asigend ,const --> Constant
// Array --> []. you can add multiple data type in array
//unshift --> add to index [0]. T


// const todos = [
//     {
//         id: 1,
//         text: 'keluar aja',
//         isCompleted: true
//     },

//     {
//         id: 2,
//         text: 'makan aja',
//         isCompleted: true
//     },
//     {
//         id: 3,
//         text: 'mandi aja',
//         isCompleted: false
//     }
// ];

// // Manipulate using forEach, map, filter
// todos.forEach(function(todo){
//     console.log(todo.text);
// });

// const todomap = todos.map(function(todo) {
//     return todo.text;
// });
// console.log(todomap);

// const todofilterCompleted = todos.filter(function(todo) {
//     return todo.isCompleted === true;
// });
// console.log(todofilterCompleted);



//===================================
//======== OBJECT ORIENTED ==========
//===================================

//construction function
// function Person(firstName, lastName, dob) {
//     this.firstName = firstName;
//     this.lastName= lastName;
//     this.dob = new Date(dob);
    //method
    // this.getBirthYear = function(){
    //     return this.dob.getFullYear();
    // }
    // this.getFullName = function(){
    //     return `${this.firstName} ${this.lastName}`;
    // }

//Prototype --> output get the construction, without function
// Person.prototype.getBirthYearPro = function(){
//     return this.dob.getFullYear();
// }
// Person.prototype.getFullNamePro = function(){
//     return `${this.firstName} ${this.lastName}`;
// }
//}

// //Class, added the metod to the class, more organized
// class Person{
//     constructor(firstName, lastName, dob){
//         this.firstName = firstName;
//         this.lastName = lastName;
//         this.dob = new Date(dob);
//     }
//     getBirthYear = function(){
//         return this.dob.getFullYear();
//         }
//     getFullName = function(){
//         return `${this.firstName} ${this.lastName}`;
//     }
// }

// // Inisiate object 
// const person1 = new Person('wina', 'ayu', '5-5-2022');
// const person2 = new Person('edwina', 'lala', '5-5-2021');
// const person3 = new Person('lidwina', 'bagas', '5-5-2000');


//console.log(person2.dob.getFullYear());

// //Modul
// console.log(person1.getBirthYear());
// console.log(person2.getFullName());
// console.log(person1);


//===================================
//============== DOM  ===============
//===================================

//console.log(window);
// //Single element 
// console.log(document.getElementById('my-form'));
// console.log(document.querySelector('h1'));


// //Multiple element
// console.log(document.querySelectorAll('.item'));
// console.log(document.getElementsByClassName('li'));

// //loop
// const items = document.querySelectorAll('.item');
// items.forEach((item) => console.log(item));

// //Changings things in dom
// const ul = document.querySelector('.items');

// // ul.remove();
// // ul.lastElementChild.remove();
// ul.firstElementChild.textContent = 'Hello';
// ul.children[1].innerText = 'Brad';
// ul.lastElementChild.innerHTML = '<h1>Hello<h1/>';

// const btn = document.querySelector('.btn');

// btn.style.background = 'blue';


// //prevent
// const btn = document.querySelector('.btn');

// btn.addEventListener('mouseout', (e) => {
//     e.preventDefault();
//     // console.log(e.target.className);
//     document.querySelector('body').classList.add('bg-dark');
//     document.querySelector('.items')
//     .lastElementChild.innerHTML = '<h1>Hello</h1>'; 
// });

const myForm = document.querySelector('#my-form');
const nameInput = document.querySelector('#name');
const emailInput = document.querySelector('#email');
const msg = document.querySelector('.msg');
const userList = document.querySelector('#users');

myForm.addEventListener('submit', onSubmit);

function onSubmit(e){
    e.preventDefault();
    if(nameInput.value === ''|| emailInput.value === ''){
        msg.classList.add('error');
        msg.innerHTML = 'Please enter all fields';
        
        setTimeout(() => msg.remove(), 3000);

    } else{
       // console.log('success');
        
       const li = document.createElement('li'); 
       li.appendChild(document.createTextNode(`${nameInput.value}: ${emailInput.value}`));

       userList.appendChild(li);
       
       //Clear fields
       nameInput.value = '';
       nameInput.value = '';
       
    }
}
