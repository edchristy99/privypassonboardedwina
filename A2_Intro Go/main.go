package main

import (
	"fmt"
	"sync"
	"time"
)



const conferenceTickets int = 50
var conferenceName = "Go Conference"
var remainingTickets uint = 50
var bookings = make([]UserData, 0)

type UserData struct{
	firstName string
	lastName string
	email string
	userTickets uint
}

var wg = sync.WaitGroup{}



func main() {
	greetUser()

	//Multiple Booking
	//we cannot mix the type in one array



	//data type has to be the same array
	//Slice: array with dynamic size
	firstName, lastName, email, userTickets := getUserInput()
	isValidName, isValidEmail, isValidUserTickets := ValidateUserInput(firstName, lastName, email, userTickets, remainingTickets)

	if isValidEmail && isValidName && isValidUserTickets {
		bookTicket(userTickets, firstName, lastName, email )
		
		wg.Add(1)
		go sendTicket(userTickets, firstName, lastName, email)
		//call func printFirstName
		firstNames := getFirstNames()
		fmt.Printf("These are all our bookings: %v\n", firstNames)

		if remainingTickets == 0 {
			//end the loop
			fmt.Println("Our Confrence s booked out.")
			//break
		}
		//continue --> skip, go to the next itteration

	} else {
		//fmt.Printf("There's only %v ticket remaining. so you can't book %v tickets\n", remainingTickets, userTickets)

		//check the wronginput
		if !isValidName {
			fmt.Println("Name is too short")
		}
		if !isValidEmail {
			fmt.Println("Email doesnt contain @")
		}
		if !isValidEmail {
			fmt.Println("invalid number of tickets")
		}

	}
	wg.Wait()

}



func greetUser() {
	fmt.Printf("Welcome to %v booking app", conferenceName)
	fmt.Printf("Total Tickets %v and %v are still avail", conferenceTickets, remainingTickets)
	fmt.Println("Get your ticket")
}

func getFirstNames() []string {
	firstNames := []string{}
	for _, booking := range bookings {
		firstNames = append(firstNames, booking.firstName)
	}
	return firstNames
}



func getUserInput() (string, string, string, uint) {
	var firstName string
	var lastName string
	var email string
	var userTickets uint
	// ask user mfor their name

	fmt.Println("Enter your first name:")
	fmt.Scan(&firstName)

	fmt.Println("Enter your last name:")
	fmt.Scan(&lastName)

	fmt.Println("Enter your email:")
	fmt.Scan(&email)

	fmt.Println("Enter your number of tickets")
	fmt.Scan(&userTickets)

	return firstName, lastName, email, userTickets

}

func bookTicket(userTickets uint, firstName string, lastName string, email string) {
	remainingTickets = remainingTickets - userTickets
	
	//create a map for user
	//canot mix datatypes!
	var userData = UserData{
		firstName: firstName,
		lastName: lastName,
		email: email,
		userTickets: userTickets,
	}
	
	// userData["firstName"] = firstName
	// userData["lastName"] = lastName
	// userData["email"] = email
	// userData["userTickets"] = strconv.FormatUint(uint64(userTickets), 10)

	bookings = append(bookings, userData)
	fmt.Printf("List of bookings %v \n", bookings)

	fmt.Printf("Thank you %v %v for booking %v tickets. \n You will receive a confirmation tickets at %v \n", firstName, lastName, userTickets, email)
	fmt.Printf("%v tickets remaining for %v \n", remainingTickets, conferenceName)

}

func sendTicket(userTickets uint, firstName string, lastName string, email string){
	
	time.Sleep(10*time.Second)
	
	var ticket = fmt.Sprintf("%v tickets for %v %v", userTickets, firstName, lastName)
	
	fmt.Println("----------------------")
	fmt.Printf("Sending ticket %v to email address %v \n", ticket, email)
	fmt.Println("----------------------")
	wg.Done()
} 
