package main

import (
	"fmt"
	"log"
	"pustaka-api/book"
	"pustaka-api/handler"

	"github.com/gin-gonic/gin"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

func main() {

	//db connection
	dsn := "root:@tcp(127.0.0.1:3306)/dbpustaka-api?charset=utf8mb4&parseTime=True&loc=Local"
	db,  err := gorm.Open(mysql.Open(dsn), &gorm.Config{})
	
	if err != nil {
		log.Fatal("db connection error")
	}
	fmt.Println("database connected")
	db.AutoMigrate(&book.Book{})
	
	
	bookRepository := book.NewRepository(db)
	// bookFileRepository := book.NewFileRepository()

	// books, err := bookRepository.FindAll()
	// for _, book := range books {
	// 	fmt.Println("Title: ", book.Title)
	// }
	bookService := book.NewService(bookRepository)

	// bookRequest := book.BookRequest{
	// 	Title:	"900 Statyri",
	// 	Price: "200000",
	// }
	// bookService.Create(bookRequest)

	bookHandler := handler.NewBookHandler(bookService)


	


	//Find Repos
	// book, err := bookRepository.FindById(3)
	// fmt.Println("Title by ID: ", book.Title)

	//Create Repos 

	// bookRepository.Create(book)

	// CRUD DB
	// //Create
	// book := book.Book{}
	// book.Title = "Atomic Habits"
	// book.Price = 80000
	// book.Discount = 10
	// book.Rating = 5
	// book.Description = "Ini adalah buku bags bengt "

	// err = db.Create(&book).Error
	// if err != nil{
	// 	fmt.Println("========================")
	// 	fmt.Println("Error creating book record")
	// 	fmt.Println("========================")
		
	// }

	// //Read
	// var books []book.Book
	// err = db.Debug().Where("rating = ?", 5).Find(&books).Error
	// if err != nil{
	// 	fmt.Println("========================")
	// 	fmt.Println("Error finding  book record")
	// 	fmt.Println("========================")
	// }

	// for _, b := range books {
	// 	fmt.Println("Title:", b.Title)
	// 	fmt.Printf("book object %v", b)
	// }
	
	// //Update
	// var book book.Book
	// err = db.Debug().Where("id = ?", 1).First(&book).Error
	// if err != nil{
	// 	fmt.Println("========================")
	// 	fmt.Println("Error finding  book record")
	// 	fmt.Println("========================")
	// }

	// book.Title = "Manusia Kelinci"
	// err = db.Save(&book).Error
	// if err != nil{
	// 	fmt.Println("========================")
	// 	fmt.Println("Error update  book record")
	// 	fmt.Println("========================")
	// }


	// //Delete
	// var book book.Book
	// err = db.Debug().Where("id = ?", 1).First(&book).Error
	// if err != nil{
	// 	fmt.Println("========================")
	// 	fmt.Println("Error finding  book record")
	// 	fmt.Println("========================")
	// }

	// err = db.Delete(&book).Error
	// if err != nil{
	// 	fmt.Println("========================")
	// 	fmt.Println("Error delete  book record")
	// 	fmt.Println("========================")
	// }
	router := gin.Default()
	v1 := router.Group("/v1")

	// v1.GET("/", bookHandler.RootHandler)
	// v1.GET("/hello", bookHandler.HelloHandler)
	// v1.GET("/books/:id/:title", bookHandler.BooksHandler)
	// v1.GET("/query", bookHandler.QueryHandler)

//	v1.POST("/books", bookHandler.PostBooksHandler)
	v1.GET("/books",bookHandler.GetBooks)
	v1.GET("/books/:id", bookHandler.GetSingleBook)
	v1.POST("/books", bookHandler.CreateBook)
	v1.PUT("/books/:id", bookHandler.UpdateBook)
	v1.DELETE("/books/:id", bookHandler.DeleteBook)

	router.Run()






}



