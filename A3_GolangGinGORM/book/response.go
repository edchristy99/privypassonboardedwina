package book

type BookResponse struct{
	ID			int 	`json:"id"`
	Title 		string 	`json:"title"`
	Price 		int 	`json:"price"`
	Description string	`json:"description"`
	Discount 	int 	`json:"discount"`
	Rating 		int		`json:"rating"`
	// SubTitle string `jsoon:"sub_title"`
}