package book

import "encoding/json"

type BookRequest struct {
	Title string 		`json:"title" binding:"required"`
	Price json.Number 	`json:"price" binding:"required,number"`
	Description string	`json:"description" binding:"required"`
	Discount json.Number `json:"discount" `
	Rating json.Number	`json:"rating" `
	// SubTitle string `jsoon:"sub_title"`
}