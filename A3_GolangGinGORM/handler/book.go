package handler

import (
	"fmt"
	"net/http"
	"strconv"

	"pustaka-api/book"

	"github.com/gin-gonic/gin"
	"github.com/go-playground/validator/v10"
)

type bookHandler struct{
	bookService book.Service
}

func NewBookHandler(bookService book.Service) *bookHandler{
	return &bookHandler{bookService}
}

func (h *bookHandler) GetBooks(c *gin.Context){
	books, err := h.bookService.FindAll()
	
	if err != nil {
		c.JSON(http.StatusBadRequest,  gin.H{
			"errors": err,
		})
		return
	}
	var booksResponse []book.BookResponse
	for _, b := range books {
		bookResponse := convertedResponse(b)

		booksResponse = append(booksResponse, bookResponse)
	}

	c.JSON(http.StatusOK, gin.H{
		"data": booksResponse,
	})

} 
func (h *bookHandler) GetSingleBook(c *gin.Context){
	idString := c.Param("id")
	id, _ := strconv.Atoi(idString)
	b,err := h.bookService.FindById(int(id))
	if err != nil {
		c.JSON(http.StatusBadRequest,  gin.H{
			"errors": err,
		})
		return
	}
	bookResponse := convertedResponse(b)

	c.JSON(http.StatusOK, gin.H{
		"data": bookResponse,
	})
	
}


func (h *bookHandler) CreateBook(c *gin.Context) {
	// get title & 
		var bookRequest book.BookRequest 
	
		err := c.ShouldBindJSON(&bookRequest)
		
		if err != nil{
			errorMessages := []string{}
			for _, e := range err.(validator.ValidationErrors) {
				errorMessage := fmt.Sprintf("Error on fields %s, condition: %s", e.Field(), e.ActualTag())
				errorMessages = append(errorMessages, errorMessage)			
				// c.JSON(http.StatusBadRequest, errorMessage)
			}
			c.JSON(http.StatusBadRequest, gin.H{
				"errors": errorMessages,
			})
			return
		}
	
		book, err := h.bookService.Create(bookRequest)
	
		if err != nil {
			c.JSON(http.StatusBadRequest,  gin.H{
				"errors": err,
			})
		}
		c.JSON(http.StatusOK, gin.H{
			"data": convertedResponse(book),
		})
	}
	func (h *bookHandler) UpdateBook(c *gin.Context) {
			// get title & 
				var bookRequest book.BookRequest 
			
				err := c.ShouldBindJSON(&bookRequest)
				
				if err != nil{
					errorMessages := []string{}
					for _, e := range err.(validator.ValidationErrors) {
						errorMessage := fmt.Sprintf("Error on fields %s, condition: %s", e.Field(), e.ActualTag())
						errorMessages = append(errorMessages, errorMessage)			
						// c.JSON(http.StatusBadRequest, errorMessage)
					}
					c.JSON(http.StatusBadRequest, gin.H{
						"errors": errorMessages,
					})
					return
				}
				idString := c.Param("id")
				id, _ := strconv.Atoi(idString)
				
				book, err := h.bookService.Update(id, bookRequest)
			
				if err != nil {
					c.JSON(http.StatusBadRequest,  gin.H{
						"errors": err,
					})
				}
				c.JSON(http.StatusOK, gin.H{
					"data": convertedResponse(book),
				})
	}	
	
	func (h *bookHandler) DeleteBook(c *gin.Context){
		idString := c.Param("id")
		id, _ := strconv.Atoi(idString)
		b,err := h.bookService.FindById(int(id))
		if err != nil {
			c.JSON(http.StatusBadRequest,  gin.H{
				"errors": err,
			})
			return
		}
		bookResponse := convertedResponse(b)
	
		c.JSON(http.StatusOK, gin.H{
			"data": bookResponse,
		})
		
	}	


// func (h *bookHandler) RootHandler(c *gin.Context) {
// 	c.JSON(http.StatusOK, gin.H{
// 		"name": "Edwina Ayu",
// 		"bio":  "A Software QA and pm",
// 	})
// }

// func (h *bookHandler) HelloHandler(c *gin.Context) {
// 	c.JSON(http.StatusOK, gin.H{
// 		"title":    "Talking avour",
// 		"subtitle": "edwina is the  preetiest",
// 	})
// }


// func (h *bookHandler) BooksHandler(c *gin.Context) {
// 	// c.JSON(http.StatusOK, gin.H{
// 	// 	"book":    "Talking avour",
// 	// 	"subtitle": "edwina is the  preetiest",
// 	// })
// 	price 	:= c.Param("price")
// 	title 	:= c.Param("title")

// 	c.JSON(http.StatusOK, gin.H{"price":price, "title":title})
// }

// func (h *bookHandler) QueryHandler(c *gin.Context) {
// 	// c.JSON(http.StatusOK, gin.H{
// 	// 	"book":    "Talking avour",
// 	// 	"subtitle": "edwina is the  preetiest",
// 	// })
// 	title 	:= c.Query("id")
// 	price 	:= c.Query("price")

// 	c.JSON(http.StatusOK, gin.H{"price":price, "title":title})
// }



func convertedResponse(b book.Book) book.BookResponse{
	return book.BookResponse{
		ID:			b.ID,
		Title: 		b.Title,
		Price: 		b.Price,
		Discount: 	b.Discount,
		Description: b.Description,
		Rating: b.Rating,
	}

}